package com.example.ziprest;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@RestController
public class ZipRestController {

    private static final String FILE1 = "/home/klaudys/Pulpit/pliczki/taryfa_dla_ciebie_30580.pdf";
    private static final String FILE2 = "/home/klaudys/Pulpit/pliczki/taryfa-inwestycje.pdf";
    private static final String FILE3 = "/home/klaudys/Pulpit/pliczki/toip-konta-osobiste.pdf";

    @GetMapping(value = "/zip", produces = "application/zip")
    public ResponseEntity getZip(final HttpServletResponse response) {
        try {
            //setting headers
            response.setStatus(HttpServletResponse.SC_OK);
            response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"test.zip\"");

            read(response.getOutputStream());
        } catch (final Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); //? zweryfikować?
            throw new IllegalStateException("AAAAA", e);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    // INTERFEJS!
    private void read(final OutputStream outputStream) throws IOException {
        try (final ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {

            // create a list to add files to be zipped
            ArrayList<File> files = new ArrayList<>();
            files.add(new File(FILE1));
            files.add(new File(FILE2));
            files.add(new File(FILE3));

            // package files
            for (File file : files) {
                //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
                zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
                try (final InputStream inputStream = new FileInputStream(file)) {

                    IOUtils.copy(inputStream, zipOutputStream);
                    zipOutputStream.closeEntry();
                }
            }
        }
    }

    //--------------------------------------------------------------
   /*
    @PostMapping("/uploadFile")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                                                            .path("/downloadFile/")
                                                            .path(fileName)
                                                            .toUriString();

        return new ResponseEntity(String.join(" - ",
                                              fileName,
                                              fileDownloadUri,
                                              file.getContentType(),
                                              String.valueOf(file.getSize())), HttpStatus.OK);
    }

    private String storeFile(MultipartFile file) {
        final Path fileStorageLocation = Paths.get("/home/klaudys/Pulpit/uploadDirectory")
                                              .toAbsolutePath()
                                              .normalize();

        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new IllegalStateException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
*/
    //------------------------------------------------------------------
    @PostMapping(value = "/zip")
    public ResponseEntity uploadZipFile(@RequestParam("file") MultipartFile file) {
        String fileName = storeZipFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                                                            .path("/downloadFile/")
                                                            .path(fileName)
                                                            .toUriString();

        return new ResponseEntity(String.join(" - ",
                                              fileName,
                                              fileDownloadUri,
                                              file.getContentType(),
                                              String.valueOf(file.getSize())), HttpStatus.OK);
    }

    private String storeZipFile(MultipartFile file) {
        byte[] buffer = new byte[1024];

        final Path fileStorageLocation = Paths.get("/home/klaudys/Pulpit/uploadDirectory")
                                              .toAbsolutePath()
                                              .normalize();

        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
//            if (fileName.contains("..")) {
//                throw new IllegalStateException("Sorry! Filename contains invalid path sequence " + fileName);
//            }

            // Copy file to the target location (Replacing existing file with the same name)
            try (final InputStream inputStream = file.getInputStream()) {

                try (final ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
                    //get the zipped file list entry
                    ZipEntry zipEntry = zipInputStream.getNextEntry();

                    while (zipEntry != null) {

                        final String currentFileName = zipEntry.getName();
                        final File newFile = new File(fileStorageLocation + File.separator + currentFileName);

                        System.out.println("file unzip : " + newFile.getAbsoluteFile());

                        //create all non exists folders
                        //else you will hit FileNotFoundException for compressed folder
                        new File(newFile.getParent()).mkdirs();

                        try (final FileOutputStream outputStream = new FileOutputStream(newFile)) {
                            // tu powinien byc zapis do storage
                            int len;
                            while ((len = zipInputStream.read(buffer)) > 0) {
                                outputStream.write(buffer, 0, len);
                            }
                        }
                        zipEntry = zipInputStream.getNextEntry();
                    }

                    zipInputStream.closeEntry();
                }

                System.out.println("Done");
            }

            return fileName;
        } catch (IOException ex) {
            throw new IllegalStateException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
